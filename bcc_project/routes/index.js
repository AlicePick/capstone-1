var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const mysql = require('mysql');
require('dotenv').config();

//Might Delete but keeping just in case.
var specase = ['[','\ ', '^', '$', '.', '|', '?', '*', '+', '(', ')', ']', '{', '}'
, '/', ':', ';', ',', '<', '>', '.', '"', '~', "`", '!', '@', '#', '%', '&',
'-', '_', '+', '='];

//Creates connection to database - Code here for testing your implementation.
//Once it works, use the db.js file to add any database implementation.
const db = mysql.createConnection({
  host: 'bccstreet.ctsj2wr8rptr.ap-southeast-2.rds.amazonaws.com',
  user: 'admin',
  password: 'ifb3992019',
  database: 'bccstreetname'
});

const gkey = process.env.GOOGLE_MAPS_API_KEY;


const googleMapsClient = require('@google/maps').createClient({
  key: gkey,
  Promise: Promise
});


//Test Connectivity:
db.connect((err) => {
  if(err){
    throw err;
  }
  console.log('Connected to DB!')
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/geocode', function(req,res, next){
  // Geocode an address.
  googleMapsClient.geocode({address: '2 George Street, Brisbane, QLD'})
  .asPromise()
  .then((response) => {
    var ad = response.json.results[0].formatted_address;
    var lat = response.json.results[0].geometry.location.lat;
    var long = response.json.results[0].geometry.location.lng;
    console.log(response.json.results[0]);
    res.render('geocode', {
      add: ad,
      la : lat,
      lo : long
    });
  })
  .catch((err) => {
    console.log(err);
  });
})

//Post any Request that has been sent through
router.post('/data', urlencodedParser, function(req, res, next) {
  var q = req.body.query;
  //Checks the query of whether or not if it has a special character.
  if (q.match(/[^a-zA-Z ]/)) {
    res.render('error', {char: q});
    return false;
  }
  // else if (q.match(/ /)){
  //   res.render('error', {char: q});
  //   return false;
  // }
  console.log(req.body.query);

  res.render('data', { dat: req.body });
});

module.exports = router;
