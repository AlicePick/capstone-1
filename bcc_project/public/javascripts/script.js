var mymap = L.map('mapid').setView([-27.47665, 153.02736], 14);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYWtoYW5xdXQiLCJhIjoiY2p0MnRrMHY4MXp6cDQ5cXh5enplMzZhZCJ9.3m7VvJVbA8YBmxu-4Coaaw'
}).addTo(mymap);

var marker = L.marker([-27.47665, 153.02736]).addTo(mymap);
var circle = L.circle([-27.4678, 153.0308], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

// var polygon = L.polygon([
//     [51.509, -0.08],
//     [51.503, -0.06],
//     [51.51, -0.047]
// ]).addTo(mymap);