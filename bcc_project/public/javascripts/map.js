var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -27.4694, lng: 153.0204},
        zoom: 13
    });
} 